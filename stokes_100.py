from dolfin import *
from mshr import *

# more detailed output
dolfin.set_log_level(dolfin.TRACE)

# Load mesh
mesh = Mesh("mesh_100.xml")

##########

# Sub domain for no-slip (mark whole boundary, inflow and outflow will overwrite)
class Noslip(SubDomain):
    def inside(self, x, on_boundary):
        return on_boundary

# Sub domain for inflow (left)
class Inflow(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] < DOLFIN_EPS and on_boundary

# Sub domain for outflow (left)
class Outflow(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] > 50 - 30*DOLFIN_EPS and on_boundary

boundaries = FacetFunction('size_t', mesh)
boundaries.set_all(3)

# Mark no-slip facets as sub domain 0
noslip = Noslip()
noslip.mark(boundaries, 0)

# Mark inflow as sub domain 1
inflow = Inflow()
inflow.mark(boundaries, 1)

# Mark outflow as sub domain 2
outflow = Outflow()
outflow.mark(boundaries, 2)

plot(boundaries)

###########


# Define function spaces
scalar = FunctionSpace(mesh, "CG", 1)
vector = VectorFunctionSpace(mesh, "CG", 2)
system = vector * scalar

# Create functions for boundary conditions
noslip = Constant((0, 0))

# No-slip boundary condition for velocity
bc0 = DirichletBC(system.sub(0), noslip, boundaries, 0)


# Collect boundary conditions
bcs = [bc0]

# Measures with references to boundaries
ds = Measure('ds', domain=mesh, subdomain_data=boundaries) #[boundaries]

# Define variational problem
p1 = 3.77e-05
p2 = 0.0
mu = 1e-09
n = FacetNormal(mesh)

(v, q) = TestFunctions(system)
(u, p) = TrialFunctions(system)

a = (-mu*inner(grad(v), grad(u)) + div(v)*p + q*div(u))*dx
L = p1*inner(v, n)*ds(1) + p2*inner(v, n)*ds(2)

# Compute solution
w = Function(system)
solve(a == L, w, bcs)
u, p = w.split()

velocity_file = File("velocity_100.xml")
velocity_file << interpolate(u, vector)


# Save solution in VTK format
#ufile_pvd = File("velocity.pvd")
#ufile_pvd << u
#pfile_pvd = File("pressure.pvd")
#pfile_pvd << p

# Plot solution
plot(u, "u")
plot(p)
interactive()

