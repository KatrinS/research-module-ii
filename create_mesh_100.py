from dolfin import *
from mshr import *

# more detailed output
dolfin.set_log_level(dolfin.TRACE)

# Define 2D geometry
domain =   Rectangle(dolfin.Point(0., 0.), dolfin.Point(50, 50)) \
 	- Circle(dolfin.Point(10, 20), 5) \
	- Circle(dolfin.Point(30, 30), 5) \
	- Circle(dolfin.Point(20, 40), 5) \



# output settings
dolfin.info("\nVerbose output of 2D geometry:")
dolfin.info(domain, True)

# Generate mesh
mesh = generate_mesh(domain, 100)

# Save mesh to file
file = File("mesh_100.xml")
file << mesh

print mesh.hmax()

# plot mesh
dolfin.plot(mesh, "2D mesh")

dolfin.interactive()




