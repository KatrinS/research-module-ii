from dolfin import *
from mshr import *

# more detailed output
dolfin.set_log_level(dolfin.TRACE)

# Define 2D geometry
domain =   Rectangle(dolfin.Point(0., 0.), dolfin.Point(50, 50)) \
         - Circle(dolfin.Point(5, 6.25), 4.99) \
	- Circle(dolfin.Point(5, 18.75), 4.99) \
	- Circle(dolfin.Point(5, 31.25), 4.99) \
	- Circle(dolfin.Point(5, 43.75), 4.99) \
         - Circle(dolfin.Point(15, 0), 4.99) \
	- Circle(dolfin.Point(15, 12.5), 4.99) \
	- Circle(dolfin.Point(15, 25), 4.99) \
	- Circle(dolfin.Point(15, 37.5), 4.99) \
	- Circle(dolfin.Point(15, 50), 4.99) \
         - Circle(dolfin.Point(25, 6.25), 4.99) \
	- Circle(dolfin.Point(25, 18.75), 4.99) \
	- Circle(dolfin.Point(25, 31.25), 4.99) \
	- Circle(dolfin.Point(25, 43.75), 4.99) \
         - Circle(dolfin.Point(35, 0), 4.99) \
	- Circle(dolfin.Point(35, 12.5), 4.99) \
	- Circle(dolfin.Point(35, 25), 4.99) \
	- Circle(dolfin.Point(35, 37.5), 4.99) \
	- Circle(dolfin.Point(35, 50), 4.99) \
         - Circle(dolfin.Point(45, 6.25), 4.99) \
	- Circle(dolfin.Point(45, 18.75), 4.99) \
	- Circle(dolfin.Point(45, 31.25), 4.99) \
	- Circle(dolfin.Point(45, 43.75), 4.99) \
# 	- Circle(dolfin.Point(10, 20), 5) \
#	- Circle(dolfin.Point(30, 30), 5) \
#	- Circle(dolfin.Point(20, 40), 5) \



# output settings
dolfin.info("\nVerbose output of 2D geometry:")
dolfin.info(domain, True)

# Generate mesh
mesh = generate_mesh(domain, 45)

# Save mesh to file
file = File("mesh.xml")
file << mesh

# plot mesh
dolfin.plot(mesh, "2D mesh")

dolfin.interactive()




