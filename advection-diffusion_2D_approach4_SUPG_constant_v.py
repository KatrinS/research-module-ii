from dolfin import *

# Load mesh 
mesh = Mesh("./mesh_100.xml")

##########

# Sub domain for inflow (left)
class Inflow(SubDomain):
    def inside(self, x, on_boundary):
        return x[0] < DOLFIN_EPS and on_boundary

boundaries = FacetFunction('size_t', mesh)
boundaries.set_all(3)

# Mark inflow as sub domain 1
inflow = Inflow()
inflow.mark(boundaries, 1)

plot(boundaries)
##########

# Create FunctionSpaces
Q = FunctionSpace(mesh, "CG", 1)
V = VectorFunctionSpace(mesh, "CG", 2)

# Create velocity Function from file
v = Function(V);
#File("velocity_100.xml") >> v
v = Constant((3e4, 0))

# Initialise source function and previous solution function
f  = Constant(0.0)
c0 = Function(Q)

# Parameters
T = 0.0015
dt = 0.00001
D = Constant((500)) 

# Test and trial functions
c, phi = TrialFunction(Q), TestFunction(Q)

# Mid-point solution
c_mid = 0.5*(c0 + c)

# Residual
r = c - c0 + dt*(dot(v, grad(c_mid)) - D*div(grad(c_mid)) - f)

# Galerkin variational problem
F = phi*(c - c0)*dx + dt*(phi*dot(v, grad(c_mid))*dx \
                      + D*dot(grad(phi), grad(c_mid))*dx)

# Add SUPG stabilisation terms
vnorm = sqrt(dot(v, v))
h = CellSize(mesh)
F += (h/(2.0*vnorm))*dot(v, grad(phi))*r*dx

# Create bilinear and linear forms
a = lhs(F)
L = rhs(F)

# Set up boundary condition
def boundary_value(n):
#	return 1.0
    if n < 100:
        return float(n)/100.0
    else:
        return 1.0

g = Constant(boundary_value(0)) 
bc = DirichletBC(Q, g, boundaries, 1)

# Assemble matrix
A = assemble(a)
bc.apply(A)

# Create linear solver and factorize matrix
solver = LUSolver(A)
solver.parameters["reuse_factorization"] = True

# Output file
out_file = File("results_approach4/concentration4.pvd")

# Set intial condition
c = c0

# Time-stepping
t = dt
while t < T:

    # Assemble vector and apply boundary conditions
    b = assemble(L)
    bc.apply(b)

    # Solve the linear system (re-use the already factorized matrix A)
    solver.solve(c.vector(), b)
    #solve(a == L, c, bc)

    # Copy solution from previous interval
    c0 = c

    # Plot solution
    plot(c)
    
    print c.vector().norm('linf')

    # Save the solution to file
    out_file << (c, t)

    # Move to next interval and adjust boundary condition
    t += dt
    g.assign(boundary_value(int(t/dt)))

# Hold plot
interactive()
